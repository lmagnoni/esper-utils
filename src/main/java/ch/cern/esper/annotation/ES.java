package ch.cern.esper.annotation;


/**
 * Custom annotation to forward EPL results to ES;
 *
 * <br/> Copyright Luca Magnoni - CERN 2015
 */
public @interface ES {

    String esindex(); //fts-ds
    String estype();  //latency 

}
