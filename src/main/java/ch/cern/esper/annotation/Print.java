package ch.cern.esper.annotation;


/**
*
* <br/> Copyright Luca Magnoni - CERN 2015
*/

public @interface Print {

    public enum Level {
        TRACE, DEBUG, INFO, WARNING, ERROR;
    }

    Level severity() default Level.INFO;

}
