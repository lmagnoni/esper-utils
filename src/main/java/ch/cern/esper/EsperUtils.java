package ch.cern.esper;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.esper.annotation.ES;
import ch.cern.esper.annotation.Print;
import ch.cern.esper.es.ESListener;
import ch.cern.esper.es.ESUtils;
import ch.cern.esper.extra.PrintListener;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;


/**
 *  Esper Utils 
 *  </br>
 *  
 *  <br/> Copyright Luca Magnoni - CERN 2015
 */

public class EsperUtils {

    // class logger
    private final static Log logger = LogFactory.getLog(EsperUtils.class);
    private final static String PROPFILE="/config.properties"; // make it name configurable from command line

    
    static { //Initialize

        try (
                InputStream stream = ESUtils.class.getResourceAsStream(PROPFILE);
                ) {
            Properties props = new Properties();
            props.load(stream);
            for(Object key: props.keySet()) {
                System.setProperty((String)key, props.getProperty((String)key));
            }
           
            logger.info("Loaded properties file [" + PROPFILE + "]");
        } catch (IOException e) {
            logger.error("Error loading " + PROPFILE, e);
            //throw new ex
        }
    }
    

   

    /**
     * Process @Subscriber and @Listeners custom annotations.
     *
     * @param statement
     * @throws Exception
     */
    private static void processStatementAnnotations(final EPStatement statement)
             { //add Exception here

        Annotation[] annotations = statement.getAnnotations();
        for (Annotation annotation : annotations) {

            if (annotation instanceof Print) {
                Print p = (Print) annotation;
                PrintListener listener = new PrintListener(
                        p.severity(), statement.getName());
                statement.addListener((UpdateListener) listener);

                logger.debug("Listener "
                        + listener.getClass().getCanonicalName()
                        + " attached to statement " + statement.getName());

            } else if (annotation instanceof ES) {
                ES es = (ES) annotation;
                ESListener listener = new ESListener(es.estype(), es.esindex(),statement.getName());
                statement.addListener((UpdateListener) listener);
                logger.debug("Listener "
                        + listener.getClass().getCanonicalName()
                        + " attached to statement " + statement.getName());
            }

        }
    }
    
    /**
     * Esper Utils entry point.
     * </br>
     * This must be called by the client code.
     * 
     * @param esper
     */
    public static void load(EPServiceProvider esper ) {
        
        for (String statement :esper.getEPAdministrator().getStatementNames() )  {
                processStatementAnnotations(esper.getEPAdministrator().getStatement(statement));
        }
        
    }


}
