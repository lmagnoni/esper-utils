package ch.cern.esper.es;


import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.searchbox.action.BulkableAction;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Bulk;
import io.searchbox.core.Bulk.Builder;
import io.searchbox.core.Index;


/**
 * 
* Configuration properties:
* 
* es.host
* es.user
* es.password
* 
* <br/> Copyright Luca Magnoni - CERN 2015
*/
public class ESUtils {    

    private final Logger logger = LoggerFactory.getLogger(ESUtils.class);
    private static JestClient client=null;

    private  String host;
    private  String user;
    private  String password;


   
    public ESUtils() {
        //Get from system property
        host = System.getProperty("es.host");
        user = System.getProperty("es.user");
        password = System.getProperty("es.password");
    }

    //better a static 1 shared?
    private void setConnection() {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(host).defaultCredentials(user, password)
                .multiThreaded(true).readTimeout(20000)
                .build());
        client = factory.getObject();
    }

    // TODO THIS NEED REFACTORING!
    //Turns a list of json events into a list of bulkable actions
    @SuppressWarnings("rawtypes")
    private List<BulkableAction> eventToBulkableAction(List<String> events) throws IOException{
        List<BulkableAction> action = new LinkedList<>();
        for(String event:events){
            try{
                action.add(new Index.Builder(event).build());
            }
            catch(Exception e){
                logger.warn("Cant make bulkable action from "+event, e);
            }
        }
        return action;
    }

    /**
     * 
     * @param events
     * @return 
     * @throws IOException
     */
    // TODO THIS NEED REFACTORING!
    public JestResult insert(List<String> events, String type, String indexName) throws IOException{
        this.setConnection();
        List<BulkableAction> eventList = eventToBulkableAction(events);
        Bulk bulk = new Bulk.Builder()
        .defaultIndex(indexName)
        .defaultType(type)
        .addAction(eventList)
        .build();
        //logger.debug("Reuquest:" + bulk.toString());
        JestResult result = client.execute(bulk);
        if (result.isSucceeded()){
            logger.debug("Document inserted "+result.getJsonString());
        }else {
            logger.warn("Insert failed: " + result.getJsonString());
        } 
        return result;
    }


    

}