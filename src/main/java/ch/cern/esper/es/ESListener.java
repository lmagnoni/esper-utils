package ch.cern.esper.es;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
*
* <br/> Copyright Luca Magnoni - CERN 2015
*/
public class ESListener implements UpdateListener {

	// class logger
	private static final Log logger = LogFactory.getLog(ESListener.class);

	// private member
	String type;
	String index;
	String statementName;
	EPServiceProvider epService;

	ESUtils esutils = new ESUtils();
	
	
	// constructor
	public ESListener(final String type, final String index, final String statementName) {
		this.index = index;
		this.type = type;
		this.statementName = statementName;
		this.epService = EPServiceProviderManager.getDefaultProvider();
	}
	
	 public String eventToJson(EventBean e) {
		    String json = epService.getEPRuntime().getEventRenderer().renderJSON("event",e).replaceAll("\n", "").replaceAll("\t", "");
		    JsonElement jelement = new JsonParser().parse(json);
		    JsonObject  jobject = jelement.getAsJsonObject();
		    String result = jobject.get("event").toString();
		    //logger.debug("result:" +result);
		    return result;
		}

	@Override
	public void update(final EventBean[] newEvents, final EventBean[] oldEvents) {
		if (newEvents != null) {
			List<String> eventsList = new LinkedList();
			for (EventBean e : newEvents) {
				String json = eventToJson(e);
				eventsList.add(eventToJson(e));
				//logger.debug(json);
			}
			try {
				logger.debug(eventsList.size()+" documents send to ES " + index);
				esutils.insert(eventsList, type, index);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	


}
