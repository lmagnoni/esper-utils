package ch.cern.esper.extra;


import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.esper.annotation.Print;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

/**
*
* <br/> Copyright Luca Magnoni - CERN 2015
*/
public class PrintListener implements UpdateListener {

    // class logger
    private static final Log logger = LogFactory.getLog(Print.class);

    // member variables
    private final Print.Level level;
    private final String statementName;
    private final EventBeanDecoder eventDecoder;

    // constructor
    public PrintListener(final Print.Level level,
            final String statementName) {
        this.level = level;
        this.statementName = statementName;
        this.eventDecoder = new EventBeanDecoder();
    }

    @Override
    public void update(final EventBean[] newEvents,
            final EventBean[] oldEvents) {
        if (newEvents != null) {
            for (EventBean e: newEvents) {
                switch (level) {
                case TRACE:
                    logger.trace(statementName + ": " + eventDecoder.decode(e));
                    break;
                case DEBUG:
                    logger.debug(statementName + ": " + eventDecoder.decode(e));
                    break;
                case INFO:
                    logger.info(statementName + ": " + eventDecoder.decode(e));
                    break;
                case WARNING:
                    logger.warn(statementName + ": " + eventDecoder.decode(e));
                    break;
                case ERROR:
                    logger.error(statementName + ": " + eventDecoder.decode(e));
                    break;
                default:
                    logger.info(statementName + ": " + eventDecoder.decode(e));
                }
            }
        } else {
            logger.error("New Events: " + newEvents + " - "
                    + "Old Events: " + oldEvents);
        }
    }

    public class EventBeanDecoder {

        public EventBeanDecoder() {

        }

        public String decode(final EventBean e) {

            //Get list of event properties

            //MIG event types have pretty-print tostring...
            // if (e.getUnderlying() instanceof ch.cern.mig.metis.model.Check) {
            //    return ((ch.cern.mig.metis.model.Check) e.getUnderlying()).toStringOneLine();
            //}

            String[] properties = e.getEventType().getPropertyNames();
            LinkedList<String> result = new LinkedList<String>();
            for (String property: properties) {
                if (e.getEventType().getPropertyType(property) != null) {
                    String type = e.getEventType().getPropertyType(property).getName();
                    Object event = e.get(property);
                    result.add(property + ": " + this.parseEventProperty(property, type, e.get(property)));
                }
            }
            //return e.getEventType().getName() + ": {" + StringUtils.join(result.toArray(), ", ") + "}";
            return "{" + StringUtils.join(result.toArray(), ", ") + "}";

        }

        /**
         * Esper return a Map of <String, Object> properties with the data
         * gathered by the EPL statements.  This function provide a smart way to
         * get a string representation of the properties,to be appended to the
         * text, xml, or HTML representation of the alert.
         * Reflection is used to introspect the Object class. Different actions
         * are taken in case it's an Array or a Set.
         *
         * @param name
         * @param type
         * @param event
         */
        @SuppressWarnings("unchecked")
        private String parseEventProperty(final String name,
                final String type,
                final Object propertyObject)  {

            /**
             * Using reflection to cast the property Object to the proper class.
             * For every type of object, the Java virtual machine instantiates an
             * immutable instance of java.lang.Class.  It can be used to access
             * the reflection API.
             */

            //logger.debug("Name: " + name + "Type: " + type);

            try {

                Class clazz;

                /**
                 * Primitive types cannot be retrieved via Class.forName.
                 * A look-up on the possible primitive types is needed.  The code
                 * leverages autoboxing capability, so that the retrieved class is
                 * directly the boxed type.
                 */

                if (type == "byte") {
                    clazz = Byte.class;
                } else if (type == "short") {
                    clazz = Short.class;
                } else if (type == "int") {
                    clazz = Integer.class;
                } else if (type == "long") {
                    clazz = Long.class;
                } else if (type == "float") {
                    clazz = Float.class;
                } else if (type == "double") {
                    clazz = Double.class;
                } else if (type == "boolean") {
                    clazz = Boolean.class;
                } else if (type == "char") {
                    clazz = Character.class;
                } else {
                    clazz = Class.forName(type);
                }

                //Deal with Object array
                if (clazz.isArray()) {

                    int len = Array.getLength(propertyObject);
                    StringWriter buffer = new StringWriter();
                    buffer.append(name + ":[ ");
                    for (int i = 0; i < len; i++) {
                        buffer.append(Array.get(propertyObject, i) + ", ");
                    }
                    buffer.append("]\n");
                    return buffer.toString();

                    //Inner properties can be Map of values. Iterate with keyset
                } else if (clazz.getSuperclass() != null
                        && clazz.getSuperclass().getName().equals("java.util.Map")) {

                    /**
                     * For performance reason I cast inner properties to a
                     * <String, Object> map, since I know Esper creates maps like
                     * that.
                     * This save the usage of reflection for calling methods, that
                     * can bring to huge performance degradation.
                     */
                    Map<String, Object> m = (Map<String, Object>) propertyObject;
                    if (m != null) {
                        StringWriter buffer = new StringWriter();
                        buffer.append(name + ":{ ");
                        for (String param:m.keySet()) {
                            buffer.append(param + ": " + m.get(param) + ", ");
                        }
                        buffer.append("}\n");
                        return buffer.toString();
                    }

                } else {
                    //Simplest case. Calling the string representation of the object itself
                    if (propertyObject != null) {
                        return clazz.cast(propertyObject).toString();
                    } else {
                        return "empty";
                    }
                }

                //throw new InvalidEventPropertyException("InvalidEventProperty:" + type);

            } catch (ClassNotFoundException e) {
                return "empty";
                //throw new InvalidEventPropertyException("ClassNotFound" + e.getMessage());
            }

            return "epmty";
        }

    }




}
